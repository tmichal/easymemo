/**
  *@author Karolina Kondzielnik, Michal Trendak
  */
#include "StatisticWidget.h"
#include "ui_StatisticWidgett.h"

StatisticWidget::StatisticWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StatisticWidget)
{
    ui->setupUi(this);
}

StatisticWidget::~StatisticWidget()
{
    delete ui;
}
