/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#include "EasyMemo.h"
#include "ui_EasyMemo.h"

EasyMemo::EasyMemo(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::EasyMemo)
{
    ui->setupUi(this);
    ui->stackedWidget->addWidget(&coursePage);
    ui->stackedWidget->addWidget(&courseChoicePage);
    ui->stackedWidget->addWidget(&statisticsPage);
    ui->stackedWidget->addWidget(&createCourse);
    connect( &courseChoicePage, SIGNAL(courseChoiceSignal(const QString)), this, SLOT(runCourse(const QString) ) );
    connect( this, SIGNAL(runCourseSignal(const QString)),&coursePage, SLOT(loadCourse( const QString  ) ) );
    connect( this, SIGNAL(loadCourseListSignal()),&courseChoicePage, SLOT(LoadCourseListSlot() ) );
    connect( this, SIGNAL(changeWhileCourseOnlineSignal(const int)),&coursePage, SLOT(onChangeSlot( const int  ) ) );
    connect( &coursePage, SIGNAL(PageChangeAccepttedSignal(const int)),this, SLOT(OnchangeAcceptedSlot(const int) ) );
    isCourseActive =false;
}

EasyMemo::~EasyMemo()
{
    delete ui;
}
void  EasyMemo::runCourse( const QString path ){
    emit    runCourseSignal(path);
    isCourseActive = true;
    ui->stackedWidget->setCurrentIndex(1);
};

void EasyMemo::on_courseBtn_clicked()
{

    if (isCourseActive== false){
        emit loadCourseListSignal();
        ui->stackedWidget->setCurrentIndex(2);
    }

}

void EasyMemo::on_addCourseBtn_clicked()
{
    if (isCourseActive== false)
        ui->stackedWidget->setCurrentIndex(4);
    else
        emit    changeWhileCourseOnlineSignal(4);
}

void EasyMemo::on_statisticBtn_clicked()
{
    if (isCourseActive== false)
        ui->stackedWidget->setCurrentIndex(3);
    else
        emit    changeWhileCourseOnlineSignal(3);


}
void EasyMemo::OnchangeAcceptedSlot(int page){
    ui->stackedWidget->setCurrentIndex(page);
    isCourseActive= false;
}
