/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#include "MemoCard.h"
#include "QDebug"

MemoCard::MemoCard()
{
    //isRepeated=false;
    ef=2.5;
    repetitionsNumber=0;
    word="brak";
    meaning="brak";
    nextRepetition=1;
    quality=-1;
    randomOrderFactor=0;
    nextRepetitionDate=(QDate::currentDate());
}

void MemoCard::setWord(std::string new_word){
    word=new_word;
}

void MemoCard::setMeaning(std::string new_meaning){
    meaning=new_meaning;
}

void MemoCard::setEF(double new_ef){
    ef=new_ef;
}

std::string MemoCard::getWord(){

    return word;

}

std::string MemoCard::getMeaning(){
    return meaning;
}

double MemoCard::getEF(){
    return ef;
}

int MemoCard::getRepetitionsNumber(){
    return repetitionsNumber;
}

void MemoCard::increaseRepetitionsNumber(){
    ++repetitionsNumber;
}

void MemoCard::setRepetitionsNumber(int number){
    repetitionsNumber=number;
}
/*
bool MemoCard::getIsRepeated(){
    return isRepeated;
}

void MemoCard::setIsRepeated(bool new_is_repeated){
    isRepeated=new_is_repeated;
}
*/
int MemoCard::getQuality(){
    return quality;
}

void MemoCard::setQuality(int new_quality){
    quality=new_quality;
}

MemoCard* MemoCard::getMemoCard()
{
    return this;
}

int MemoCard::getNextRepetition(){
    return nextRepetition;
}

void MemoCard::setNextRepetition(int new_next_repetition){
    nextRepetition=new_next_repetition;
}
void MemoCard::decreaseQuality(){
    this->quality--;
}
void MemoCard::increaseQuality(){
    this->quality++;
}

void MemoCard::setRandomOrderFactor(int random_order_factor){
    randomOrderFactor=random_order_factor;
}
double MemoCard::getRandomOrderFactor(){
    return randomOrderFactor;
}

void MemoCard::setNextRepetitionDate(QDate next_repetition_date){
    nextRepetitionDate=next_repetition_date;
}
QDate MemoCard::getNextRepetitionDate(){
    return nextRepetitionDate;
}

void MemoCard::editNextRepetitionDate(int next_repetition){
    setNextRepetitionDate(QDate::currentDate().addDays(next_repetition));

}

