/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#ifndef FILEUTILITY_H
#define FILEUTILITY_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <QtXml/QtXml>
#include <MemoCourse.h>
#include <Course.h>
#include <iostream>
#include <QDate>


/**
 * @brief The FileUtility class inhritates from QObject. Class responsible for correct handling of external XML files.
 */
class FileUtility : public QObject
{
    Q_OBJECT
    /**
     * @brief document represents XML document object.
     */
    QDomDocument document;
    /**
     * @brief root represents document tree element.
     */
    QDomElement  root;
    /**
     * @brief filePath stores the path to the file.
     */
    QString filePath;



public:
    /**
    * @brief FileUtility default constructor.
    */
    FileUtility();
    /**
     * @brief FileUtility explicit parameter constructor.
     * @param file_path reference to the file path.
     * @param node node name in the XML file.
     * @param parent default argument.
     */
    explicit FileUtility(const QString &file_path, std::string node, QObject *parent = nullptr);

    /**
     * @brief getCourseFromFile responsible for loading pairs of word meaning pairs from the XML file.
     * @return vector of MemoCard objects.
     */
    std::vector<MemoCard> getCourseFromFile();
    /**
     * @brief getCoursesListFromFile responsible for loading list of courses from the XML file.
     * @return vector of Course objects.
     */
    std::vector<Course> getCoursesListFromFile();
    /**
     * @brief getConfigFromFile responsible for loading configuration data from the XML file - UNUSED.
     */
    void getConfigFromFile();
    /**
     * @brief saveToFile responsible for writing our data to an XML file.
     */
    void saveToFile();
    /**
     * @brief addMemoCardToFile allows to add word meaning pair to the QDomDocument object.
     * @param memo new memo card;
     */
    void addMemoCardToFile(MemoCard* memo);
    /**
     * @brief returnIndexIfExists allows to get index of an element in QDomNodeList if exists in XML file.
     * @param element value to check if exists.
     * @param tag branch in XML file.
     * @param attribute attribute in XML file
     * @return
     */
    int returnIndexIfExists(std::string &element, std::string tag, std::string attribute);
    /**
     * @brief editMemoCardInFile allows to edit existing card in file
     * @param word a word to change.
     * @param meaning meaning of the word to change.
     * @param ef new ef value.
     * @param next_repetition new number of days to the next repetition.
     * @param repetitions_number new number of repetitions of one card.
     * @param next_repetition_date new date of the next repetition.
     */
    void editMemoCardInFile(std::string word, std::string meaning, double ef, int next_repetition, int repetitions_number, QDate next_repetition_date);
    /**
     * @brief editCourseInFile allows to edit data about existing course.
     * @param name name of the course to change.
     * @param time new learning time.
     * @param path path of the course.
     */
    void editCourseInFile(std::string name, QTime time, std::string path);
    /**
     * @brief addCourse allows to add new course to the list of courses.
     * @param course new course.
     */
    void addCourse(MemoCourse* course);
};

#endif // FILEUTILITY_H
