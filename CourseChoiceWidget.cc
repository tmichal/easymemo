/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#include "Config.h"
#include "CourseChoiceWidget.h"
#include "ui_CourseChoiceWidget.h"
#include <QMessageBox>

CourseChoiceWidget::CourseChoiceWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CourseChoiceWidget)
{
    ui->setupUi(this);

}

CourseChoiceWidget::~CourseChoiceWidget()
{
    delete ui;
}



void CourseChoiceWidget::on_RepeatBtn_clicked()
{
    QList<QListWidgetItem*> items = ui->CourseList->selectedItems();
    if(items.length() == 1)
    {
        int tmp = ui->CourseList->currentRow();

        emit    courseChoiceSignal(QString::fromStdString(courseList[ static_cast<unsigned int>(tmp)].getCourse()->getFilePath()));
    }
    else
    {
        QMessageBox::warning(this, "Błąd!", "Nie Wybrałes Kursu");
    }
}

void CourseChoiceWidget::on_AddFromFileBtn_clicked()
{

}

void CourseChoiceWidget::on_MixedRepeadBtn_clicked()
{

}
void CourseChoiceWidget::LoadCourseList()
{
    for(auto course: courseList){
        ui->CourseList->addItem(QString::fromStdString(course.getCourse()->getCourseName()));

    }
}

void CourseChoiceWidget::LoadCourseListSlot()
{
    FileUtility fileutility("courseslist.xml","courseslist");
    courseList = fileutility.getCoursesListFromFile();
    ui->CourseList->clear();
    for(auto course: courseList){
        ui->CourseList->addItem(QString::fromStdString(course.getCourse()->getCourseName()));

    }
}

