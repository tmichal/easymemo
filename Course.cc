/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#include "Course.h"

Course::Course()
{

}

Course::Course(std::string file_path, std::string course_name) : courseName(course_name), courseFilePath(file_path){}

std::string Course::getFilePath(){
    return courseFilePath;
}

std::string Course::getCourseName(){
    return courseName;
}

void Course::setFilePath(std::string file_path){
    courseFilePath = file_path;
}

void Course::setCourseName(std::string course_name){
    courseName = course_name;
}

void Course::setLearningTime(QTime learning_time){
    learningTime=learning_time;
}
QTime Course::getLearningTime(){
    return learningTime;
}

void Course::setNumberOfCards(int number_of_cards){
    numberOfCards=number_of_cards;
}

int Course::getNumberOfCards(){
    return numberOfCards;
}

Course* Course::getCourse()
{
    return this;
}
