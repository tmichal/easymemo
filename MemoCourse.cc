/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#include "MemoCourse.h"
#include "FileUtility.h"

MemoCourse::MemoCourse(std::string file_path, std::string course_name)
{
    courseName=course_name;
    courseFilePath=file_path;
    FileUtility fileutility(QString::fromStdString(courseFilePath), "memocards");
    memoCards = fileutility.getCourseFromFile();
    qDebug()<<"MemoCourse completed";
    secondRepeticion = false;

}

MemoCourse::MemoCourse(){}

void MemoCourse::LoadSingleCourse(std::string file_path, std::string course_name)
{
    courseFilePath=file_path;
    courseName=course_name;
    FileUtility fileutility(QString::fromStdString(courseFilePath), "memocards");
    memoCards = fileutility.getCourseFromFile();

}

MemoCard* MemoCourse::getMemoCard(int index)
{
    return memoCards[index].getMemoCard();
}
int MemoCourse::ReturnSize()
{
    return this->memoCards.size();
}

void MemoCourse::findRepetitions(){
    QDate date(QDate::currentDate());
    srand( time( NULL ) );
    for(auto &card: memoCards){
        if(card.getNextRepetitionDate()<=date){
            card.setRandomOrderFactor(rand());
        }else{
            card.setRandomOrderFactor(-10);
        }
    }
    sort(memoCards.begin(), memoCards.end(), sortByRandomOrderFactor);

}


bool MemoCourse::sortByRandomOrderFactor(MemoCard a, MemoCard b){
    return a.getRandomOrderFactor()>b.getRandomOrderFactor();
}

void MemoCourse::editAndSave(std::string file_path){
    FileUtility fileutility(QString::fromStdString(file_path), "memocards");
    for(auto card: memoCards){
        fileutility.editMemoCardInFile(card.getWord(),card.getMeaning(),card.getEF(),card.getNextRepetition(),card.getRepetitionsNumber(),card.getNextRepetitionDate());
    }
    fileutility.saveToFile();
}
void MemoCourse::addMemoCard(MemoCard tmp){
    memoCards.push_back(tmp);
}
