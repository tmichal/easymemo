/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#ifndef MEMOCOURSE_H
#define MEMOCOURSE_H
#include<vector>
#include<ctime>
#include"MemoCard.h"
#include"Course.h"
#include<algorithm>

/**
 * @brief The MemoCourse class cointains one memo course.
 */
class MemoCourse : public Course
{
public:
    /**
     * @brief MemoCourse parametric constructor, allows to create new course.
     * @param file_path string path to the file.
     * @param course_name string name of the course.
     */
    MemoCourse(std::string file_path, std::string course_name);
    /**
     * @brief getMemoCard allows to get memo card by index.
     * @param index index of geting Memocard
     * @return MemoCard pointer
     */
    MemoCard* getMemoCard(int index);
    /**
     * @brief MemoCourse default constructor.
     */
    MemoCourse();
    /**
     * @brief LoadSingleCourse allows to get from XML file single memo course.
     * @param file_path a path to XML file with course element.
     * @param course_name name of memo course.
     */
    void LoadSingleCourse(std::string file_path, std::string course_name);
    /**
     * @brief ReturnSize return size of a MemoCard vector
     * @return size of a MemoCourse Vector
     */
    int ReturnSize();
    /**
     * @brief findRepetitions allows to find pairs word and meaning that need a repetition. If need, pair gets random non-negative value,if not, pair gets value -10.
     */
    void findRepetitions();
    /**
     * @brief sortByRandomOrderFactor allows to sort memo cards by random order factor (value from findRepetitions()).
     * @param a first memo card to compare.
     * @param b second memo card to compare.
     * @return result of the comparison
     */
    static bool sortByRandomOrderFactor(MemoCard a, MemoCard b);
    /**
     * @brief editAndSave allows to save changed course.
     * @param file_path path to the file.
     */
    void editAndSave(std::string file_path);
    /**
     * @brief addMemoCard allows to add new memo cart to the memo course.
     * @param tmp new memo card to add.
     */
    void addMemoCard(MemoCard tmp);
private:
    /**
     * @brief memoCards vector of word meaning pairs.
     */
    std::vector<MemoCard> memoCards;
    /**
     * @brief secondRepeticion  bool value specifying first/second repeticion, false - first repetiction
     */
    bool secondRepeticion;


};

#endif // MEMOCOURSE_H
