/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#include "Algorithm.h"

Algorithm::Algorithm(){}

Algorithm& Algorithm::getInstance()
{
    static Algorithm instance;
    return instance;
}

double Algorithm::countEF(double const &old_EF, int &quality){
    double newEF;
    newEF=old_EF+(0.1-(5-quality)*(0.08+(5-quality)*0.02));
    if (newEF<1.3){
        newEF=1.3;
    }
    return newEF;
}

void Algorithm::calculateNextRepetition(MemoCard &memo, int &quality){
    int nextRepetition;
    memo.increaseRepetitionsNumber();

    qDebug() << memo.getNextRepetition();
    qDebug() << memo.getEF();
    switch(memo.getRepetitionsNumber()){
    case 1:
        nextRepetition=1;
        memo.setEF(2.5);
        break;
    case 2:
        nextRepetition=6;
        memo.setEF(2.5);
        break;
    default:
        double ef=countEF(memo.getEF(),quality);
        memo.setEF(ef);
        if (ef<3){
            nextRepetition=1;
            memo.setRepetitionsNumber(0);
        }else
            nextRepetition=static_cast<int>(memo.getNextRepetition()*ef);
        break;
    }
    memo.setNextRepetition(nextRepetition);
    memo.editNextRepetitionDate(nextRepetition);
}
