/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#include "FileUtility.h"



FileUtility::FileUtility(const QString &file_path, std::string node, QObject *parent) : QObject(parent), filePath(file_path)
{

    QFile fileOpenXML(file_path);

    bool allIsLoaded = false;

    if(fileOpenXML.open(QIODevice::ReadOnly | QIODevice::Text)){
        qDebug() << "Database:" << file_path << "opened";
        if(document.setContent(&fileOpenXML)){
            qDebug() << "Database document readed correctly";

            root = document.firstChildElement();
            if(root.nodeName() == QString::fromStdString(node)){
                allIsLoaded = true;
            }
        }
        fileOpenXML.close();

    }
    if(!allIsLoaded){
        root = document.createElement(QString::fromStdString(node));
        document.appendChild(root);
    }
}

std::vector<MemoCard> FileUtility::getCourseFromFile()
{
    std::vector<MemoCard> memoCards;
    QDomNodeList nodeList = root.elementsByTagName("memocard");
    qDebug() << "Course from file";
    QDomElement cardElement;
    MemoCard localMemoCard;
    for(int i = 0; i < nodeList.count(); ++i){
        cardElement = nodeList.at(i).toElement();

        qDebug() << "Word:";
        qDebug() << cardElement.attribute("word");
        localMemoCard.setWord(cardElement.attribute("word").toStdString());

        qDebug() << "Meaning:";
        qDebug() << cardElement.attribute("meaning");
        localMemoCard.setMeaning(cardElement.attribute("meaning").toStdString());

        qDebug() << "EF:";
        qDebug() << cardElement.attribute("ef");
        localMemoCard.setEF(cardElement.attribute("ef").toDouble());

        qDebug() << "Next repetition:";
        qDebug() << cardElement.attribute("nextRepetition");
        localMemoCard.setNextRepetition(cardElement.attribute("nextRepetition").toInt());

        qDebug() << "Next repetition date:";
        qDebug() << cardElement.attribute("nextRepetitionDate");
        localMemoCard.setNextRepetitionDate(QDate::fromString(cardElement.attribute("nextRepetitionDate"),"dd-MM-yyyy"));

        qDebug() << "Repetitions number:";
        qDebug() << cardElement.attribute("repetitionsNumber");
        localMemoCard.setRepetitionsNumber(cardElement.attribute("repetitionsNumber").toInt());

        memoCards.push_back(localMemoCard);
    }

    return memoCards;
}

std::vector<Course> FileUtility::getCoursesListFromFile(){
    std::vector<Course> coursesList;
    QDomNodeList nodeList = root.elementsByTagName("course");
    qDebug() << "Courses list from file";
    QDomElement courseElement;
    Course localCourse;
    for(int i = 0; i < nodeList.count(); ++i){
        courseElement = nodeList.at(i).toElement();

        qDebug() << "Name:";
        qDebug() << courseElement.attribute("name");
        localCourse.setCourseName(courseElement.attribute("name").toStdString());

        qDebug() << "Path:";
        qDebug() << courseElement.attribute("path");
        localCourse.setFilePath(courseElement.attribute("path").toStdString());

        qDebug() << "Number of cards:";
        qDebug() << courseElement.attribute("numberOfCards");
        localCourse.setNumberOfCards(courseElement.attribute("numberOfCards").toInt());

        qDebug() << "Learning time:";
        qDebug() << courseElement.attribute("time");
        localCourse.setLearningTime(QTime::fromString(courseElement.attribute("time"),"hh:mm:ss"));

        coursesList.push_back(localCourse);
    }
    return coursesList;
}


void FileUtility::getConfigFromFile(){
    QDomNodeList nodeList = root.elementsByTagName("config");
    qDebug() << "Config data from file";
    QDomElement configElement;
}

void FileUtility::saveToFile()
{
    QFile fileSaveXML(filePath);


    if(fileSaveXML.open(QIODevice::WriteOnly | QIODevice::Text)){
        fileSaveXML.write(document.toString().toLatin1());
        qDebug()<<"File saved";
        fileSaveXML.close();
    }
}

int FileUtility::returnIndexIfExists(std::string &element, std::string tag, std::string attribute){
    QDomNodeList nodeList = root.elementsByTagName(QString::fromStdString(tag));

    QDomElement domElement;

    for(int i = 0; i < nodeList.count(); ++i){
        domElement = nodeList.at(i).toElement();
        if(domElement.attribute(QString::fromStdString(attribute)) == QString::fromStdString(element)){
            return i;
        }
    }
    return -1;
}

void FileUtility::addMemoCardToFile(MemoCard* memo)
{
    QDomElement cardElement = document.createElement("memocard");
    cardElement.setAttribute("word", QString::fromStdString(memo->getWord()));
    cardElement.setAttribute("meaning", QString::fromStdString(memo->getMeaning()));
    cardElement.setAttribute("ef",QString::number(memo->getEF()));
    cardElement.setAttribute("nextRepetition", QString::number(memo->getNextRepetition()));
    cardElement.setAttribute("repetitionsNumber", QString::number(memo->getRepetitionsNumber()));
    cardElement.setAttribute("nextRepetitionDate", memo->getNextRepetitionDate().toString("dd-MM-yyyy"));
    root.appendChild(cardElement);
}

void FileUtility::editMemoCardInFile(std::string word, std::string meaning, double ef, int next_repetition, int repetitions_number, QDate next_repetition_date)
{
    QDomNodeList nodeList = root.elementsByTagName("memocard");

    QDomElement cardElement;

    int index = returnIndexIfExists(word, "memocard", "word");
    if(index>=0){
        cardElement = nodeList.at(index).toElement();
        cardElement.setAttribute("ef", QString::number(ef));
        cardElement.setAttribute("nextRepetition", QString::number(next_repetition));
        cardElement.setAttribute("repetitionsNumber", QString::number(repetitions_number));
        cardElement.setAttribute("nextRepetitionDate", next_repetition_date.toString("dd-MM-yyyy"));

    }
}

void FileUtility::editCourseInFile(std::string name, QTime time, std::string path)
{
    QDomNodeList nodeList = root.elementsByTagName("course");

    QDomElement courseElement;

    int index = returnIndexIfExists(name, "course", "name");
    if(index>=0){
        courseElement = nodeList.at(index).toElement();
        courseElement.setAttribute("path", QString::fromStdString(path));
    }
}

void FileUtility::addCourse(MemoCourse* course)
{
    QDomElement courseElement = document.createElement("course");
    courseElement.setAttribute("name", QString::fromStdString(course->getCourseName()));
    courseElement.setAttribute("path", QString::fromStdString(course->getFilePath()));
    courseElement.setAttribute("time", course->getLearningTime().toString("hh:mm:ss"));
    root.appendChild(courseElement);
}
